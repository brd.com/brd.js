import API, { getItem, setItem, removeItem } from './api'
import PigeonProto from './eme-proto'
import { onceReady, prepareForOnceReady } from './utils';
import { Buffer } from 'buffer';
const PublicKey = require('bitcore-lib/publickey');

import Emitter from './emitter';
import deepMap from './deep-map'

const elliptic = require('elliptic')
const chacha = require('chacha')
const base58 = require('bitcore-lib/encoding/base58')
const uuid = require('uuid/v4')


const Random = require('bitcore-lib/crypto/random')
const ECDSA = require('bitcore-lib/crypto/ecdsa')
const Hash = require('bitcore-lib/crypto/hash')
const Signature = require('bitcore-lib/crypto/signature')



function EME(api,service) {
  if(api instanceof API) this.api = api;
  
  else {
    this.api = new API(api);
  }

  this.service = service;
    
  Emitter.mixin(this);

  prepareForOnceReady.call(this,async function() {
    // wait for API to be ready:
    await this.api.ready();
    
    // read whatever we need out of our persistent store:
    await readPersistent.call(this);
  });
}

const C = {
  log: function() {
    // console.log.apply(this,arguments);
  },
  error: function() {
    console.error.apply(this,arguments)
  }
};

EME.prototype.proto = PigeonProto.messages;
EME.proto = PigeonProto.messages;

EME.prototype.Buffer = Buffer;

EME.prototype.destroy = async function () {
  this._latch.reject(new Error('destroyed api object'));
  
  // if ready is false, we won't call through:
  this._ready = false;
  
  // create a stub latch that always rejects everything:
  this._latch = { promise: () => new Promise((res,rej) => rej(new Error('destroyed api object'))) };

  this.associatedKeys = null;
  this.entries = null;
  this.envelopes = null;

  await removeItem.call(this.api,'brd-eme');
}

Object.assign(EME.prototype,onceReady({
  async fetch() {
    var after = '';

    if(this.entries.length) {
      after = '?after='+this.entries[this.entries.length-1].cursor;
    }

    var { error, result } = await this.api.fetch('/inbox'+after);

    if(error) throw new Error(error);

    var entries = result.entries;
    for(var i in entries) {
      var entry = entries[i];

      receive.call(this,entry);
    }

    await writePersistent.call(this);
  },

  base58Pubkey() {
    return base58.encode(this.api.privateKey.toPublicKey().toBuffer())
  },

  async ack(entries) {
    var cursors ;
    if(typeof entries.map == 'function')
      cursors = entries.map(e => e.cursor);
    else
      cursors = [entries.cursor];

    var { error , result } = await this.api.fetch('/ack',{
      method: 'POST',
      body: cursors,
    })

    
    if(error) {
      debugger;
      throw new Error(error.statusCode);
    }

    //      if(res.num_acked == entries.length) {
    cursors.forEach(cursor => {
      var e = this.entries.find(e => e.cursor == cursor)
      e.acknowledged = true;
      e.acknowledged_time = (new Date()).toUTCString();
    })

    await writePersistent.call(this);
    
    //      } else {
    //        throw new Error("Failure to ACK");
    //      }

    return true;
  },

  async send(toPubkey,type,message,options={}) {
    var identifier = options.identifier || uuid();

    // hack to get around backend serialization bug:

    let pushNotification;
    var pn = options.pushNotification;
    
    if(pn) {
      pushNotification = {
        message: pn.message || '',
        title: pn.title || '',
        action: pn.action || '',
      };
    }

/*    console.log("SENDING ",{
      to: toPubkey.toString('base64'),
      service: this.service,
      type: type,
      message: message,
      options,
      pushNotification
    }); */
    
    var body = construct.call(this,toPubkey,this.service,type,message,Object.assign({ identifier },{
      ...options,
      pushNotification
    }));

    var hash = {
      method: 'POST',
      headers: {
        'content-type': 'application/x-protobuf',
        'accept': 'application/json',
      },
      body: body,
    };

    var res = await this.api.fetch('/message',hash);

    return identifier;
  },

  async associateKey(privateKey) {
    const pubkey = base58.encode(new PublicKey(privateKey).toDER());
    await this.api.fetch('/me/associated-keys',{
      method: 'POST',
      body: pubkey,
      headers: { 'content-type': 'plain/text' },
    });

    this.associatedKeys[pubkey] = privateKey;
    await writePersistent.call(this);
  },

  qrLinkUrl() {
    var publicKeyHex = this.api.privateKey.toPublicKey().toBuffer().toString('hex');
    var id = this.api.options && this.api.options.applicationID || this.api.deviceID;
    var service = this.service;
    
    return `https://brd.com/x/link-wallet?publicKey=${publicKeyHex}&id=${id}&service=${service}`;
  },

  deepLinkUrl() {
    var publicKeyHex = this.api.privateKey.toPublicKey().toBuffer().toString('hex');
    var id = this.api.options && this.api.options.applicationID || this.api.deviceID;
    var service = this.service;
    
    return `https://brd.com/x/link-wallet?publicKey=${publicKeyHex}&id=${encodeURIComponent(id)}&service=${encodeURIComponent(service)}&return-to=${encodeURIComponent(document.location.href)}`;
  },

  entriesOfType(type,callback) {
    if(typeof type != 'string') throw new Error("Exptected first parameter to be a string");
    if(callback && typeof callback != 'function') throw new Error("Expected second parameter to be a function");
    
    return this.entries.filter(e => (
      e.envelope.messageType.toLowerCase() == type.toLowerCase() &&
        (!callback || callback(e))
    ));
  },
  
  linkedPubkey(options={}) {
    var linkEntries = this.entries.filter((m,i) => m.envelope.messageType == 'LINK' && (m.acknowledged || !options.acknowledged) && m.envelope.message.status == PigeonProto.messages.Status.ACCEPTED);

    if(linkEntries.length == 0) return null;
    var last = linkEntries[linkEntries.length-1];
    return last && last.envelope && last.envelope.message.publicKey;
  },

  setPollInterval(interval) {
    if(this._pollInterval == interval) return;

    clearInterval(this._pollId);
    this._pollInterval = interval;

    if(interval) {
      console.log("Setting poll interval to ",interval);
      this._pollId = setInterval(() => pollLoop.call(this),this._pollInterval);
    }
  },
  
  startPolling() {
    if(!this._poll) this._poll = 0;
    this._poll++;
    if(this._poll > 0) {
      // start with a poll interval of 2k:
      if(!this._pollInterval) this.setPollInterval(2000);
    }
  },
  
  stopPolling() {
    this._poll--;
    if(this._poll <= 0) {
      this.setPollInterval(0);
    }
    
    return true;
  },

}));

const deepBufferize = deepMap((v) => v instanceof Uint8Array ? Buffer.from(v) : undefined);

function toSnakeCase(uppercase) {
  var parts = uppercase.split('_');
  return parts.map(p => (p[0]?p[0].toUpperCase():'') + p.slice(1).toLowerCase()).join('')
}


function verify(signature,buffer,publicKey) {
  var EC = new elliptic.ec('secp256k1');
  var key = EC.keyFromPublic(publicKey);

  var hashbuf = Hash.sha256sha256(buffer)

  return key.verify(hashbuf,signature);
}

function joinBuffers() {
  var buffers = arguments;
  return Buffer.from(buffers,buffers.reduce((a,b)=>a+b.length,0));
}

const sign = (privateKey,buffer) => {
  var hashbuf = Hash.sha256sha256(buffer)

  var ecdsa = new ECDSA();

  ecdsa.hashbuf = hashbuf;
  ecdsa.privkey = privateKey;
  ecdsa.pubkey = privateKey.toPublicKey();
  ecdsa.signRandomK();
  ecdsa.calci();

  return Buffer.from(ecdsa.sig.toCompact());
}

function decrypt(encrypted_message,key,nonce) {
  var body = encrypted_message.slice(0,encrypted_message.length - 16);
  var tag = encrypted_message.slice(encrypted_message.length - 16);

  var decipher =  chacha.createDecipher(key, nonce);
  decipher.setAuthTag(tag);

  var a = decipher.update(body);
  var b = decipher.final();

  return Buffer.concat([a,b],a.length+b.length);
}

function encrypt(key,message) {
  var nonce = Random.getRandomBuffer(96/8);

  var cipher =  chacha.createCipher(key, nonce);

  var encrypted = cipher.update(message);
  var rest = cipher.final();
  var tag = cipher.getAuthTag();

  return {
    nonce: nonce,
    encryptedMessage: Buffer.concat([encrypted,rest,tag],encrypted.length + rest.length + tag.length),
  };
}

function serialize(value,key) {
  var obj = deepMap((v,k) => {
    // var Buf = Buffer;
    //if(k == 'senderPublicKey') debugger;
    return Buffer.isBuffer(v) ? { type:'Buffer', data: Array.prototype.slice.call(v) } : undefined
  },value);
  return JSON.stringify(obj);
}

function deserialize(string,key) {
  var hash = string && JSON.parse(string);
  return deepMap((v) => (v && typeof v == 'object' && v.type == 'Buffer')?Buffer.from(v.data):undefined,hash);
}

function sharedKey(otherPublic,ourPublic) {
  var EC = new elliptic.ec('secp256k1');

  var publicKey  = EC.keyFromPublic(otherPublic);
  var privateKey = EC.keyFromPrivate(this.api.privateKey.toString());

  if(ourPublic) {
    var associatedPrivateKey = this.associatedKeys[base58.encode(ourPublic)];
    if(associatedPrivateKey) {
      privateKey = EC.keyFromPrivate(associatedPrivateKey.toString());
    }
  }

  var ecdh = privateKey.derive(publicKey.getPublic()).toBuffer();
  var hashed = Hash.sha256(ecdh);

  C.log("SHARED KEY FOR: ",otherPublic);
  C.log("  -> ",hashed.toString('base64'));

  return hashed;
}

export function construct(toPubkey,service,type,message,options) {
  var ProtobufType = PigeonProto.messages[toSnakeCase(type)];
  var error = ProtobufType.verify(message);
  if(error) throw new Error(error);

  var messageBytes = Buffer.from(ProtobufType.encode(message).finish());
  var key = sharedKey.call(this,toPubkey);

  var envelope = Object.assign(
    {
      version: 1,
      service: service,
      messageType: type,
      senderPublicKey: this.api.privateKey.toPublicKey().toDER(),
      receiverPublicKey: toPubkey,
      identifier: uuid(),
      signature: Buffer.from('','hex'),
    },
    encrypt(key,messageBytes),
    options,
  );


  error = PigeonProto.messages.Envelope.verify(envelope)
  if(error) throw new Error(error);

  var toSign = Buffer.from(PigeonProto.messages.Envelope.encode(envelope).finish());
  C.log("SIGNING: ",Object.assign({},envelope),toSign.toString('base64'));
  envelope.signature = sign(this.api.privateKey,toSign);
  C.log("signature: ",envelope.signature.toString('base64'));
  C.log("SIGNED: ",JSON.stringify(envelope));
  return Buffer.from(PigeonProto.messages.Envelope.encode(envelope).finish());
}

export function deconstruct(buffer) {
  var envelope = deepBufferize(PigeonProto.messages.Envelope.decode(buffer));
  C.log("ENvy: ",JSON.stringify(envelope));

  var key = sharedKey.call(this,
                           Buffer.from(envelope.senderPublicKey),
                           Buffer.from(envelope.receiverPublicKey),
                          );

  var ProtobufType = PigeonProto.messages[toSnakeCase(envelope.messageType)];

  var signature = Signature.fromCompact(Buffer.from(envelope.signature));
  envelope.signature = Buffer.from('','hex');
  var signedData = Buffer.from(PigeonProto.messages.Envelope.encode(envelope).finish());
  C.log("CHECKING ",Object.assign({},envelope),signedData.toString('base64'));

  var ecdsa = new ECDSA();
  ecdsa.hashbuf = Hash.sha256sha256(signedData);
  ecdsa.sig = signature;

  var announced = base58.encode(envelope.senderPublicKey);
  var calculated = base58.encode(ecdsa.toPublicKey().toDER());

  if(announced != calculated) {
    C.error(`Invalid envelope signature ${announced} != ${calculated}`);
    // TODO: This is temporarily disabled to help with debugging:
    // throw new Error('invalid-signature');
  }

  // TODO: Also temporarily disabled:
  let messageObject;

  try {
    var message = decrypt(Buffer.from(envelope.encryptedMessage),key,Buffer.from(envelope.nonce));
    messageObject = deepBufferize(ProtobufType.decode(Buffer.from(message)));
  } catch(x) {
    C.error("Error decrypting: ",x);
  }

  return Object.assign({},envelope,{ message: messageObject });
};


function receive(entry) {
  if(this.entries.find((e) => e.cursor == entry.cursor)) {
    // we already have it!
    return;
  }

  var buffer = Buffer.from(entry.message,'base64');
  C.log("RECEIVING BODY: ",buffer.toString('base64'));

  var envelope = deconstruct.call(this,buffer);

  C.log("ENVELOPE: ",envelope);

  var entryWithEnvelope = Object.assign({},entry,{ envelope: envelope });
  this.envelopes.push(envelope);
  this.entries.push(entryWithEnvelope);

  try {
    this.emit('message',envelope.message,envelope,entryWithEnvelope);
    this.emit(envelope.messageType.toLowerCase(),envelope.message,envelope,entryWithEnvelope);
  } catch(x) {
    C.error("Error emitting message events");
    C.error(x);
  }
};

async function writePersistent() {
  var hash = {
    entries: this.entries,
    associatedKeys: this.associatedKeys,
  }
  
  return await setItem.call(this.api,'brd-eme',serialize(hash));
}

async function readPersistent() {
  var serialized = await getItem.call(this.api,'brd-eme');

  if(serialized) {
    try {
      var hash = deserialize(serialized);

      this.entries = hash.entries;
      this.envelopes = this.entries.map(e => e.envelope);
      
      this.associatedKeys = hash.associatedKeys;
    } catch(x) {
      C.error("Error reading persistent store: ",x);
    }
  }
  
  this.entries = this.entries || [];
  this.envelopes = this.envelopes || [];
  this.associatedKeys = this.associatedKeys || [];
}

async function pollLoop() {
  // if we have overlapping poll Loops, we're polling too fast,
  // slow down, if we're not overlapping, speed up:
  if(this._fetching >= 1) {
    this.setPollInterval(Math.min(this._pollInterval*2,10000));
  } else if(this._fetching == 0) {
    this.setPollInterval(Math.max(this._pollInterval/2,1500));
  }
  
  this._fetching++;
  await this.fetch();
  this._fetching--;
}

export default EME;

