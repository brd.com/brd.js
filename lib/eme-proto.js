/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const messages = $root.messages = (() => {

    const messages = {};

    messages.PushNotification = (function() {

        function PushNotification(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        PushNotification.prototype.message = "";
        PushNotification.prototype.action = "";
        PushNotification.prototype.title = "";

        PushNotification.create = function create(properties) {
            return new PushNotification(properties);
        };

        PushNotification.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            w.uint32(10).string(m.message);
            if (m.action != null && m.hasOwnProperty("action"))
                w.uint32(18).string(m.action);
            if (m.title != null && m.hasOwnProperty("title"))
                w.uint32(26).string(m.title);
            return w;
        };

        PushNotification.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        PushNotification.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.PushNotification();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.message = r.string();
                    break;
                case 2:
                    m.action = r.string();
                    break;
                case 3:
                    m.title = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("message"))
                throw $util.ProtocolError("missing required 'message'", { instance: m });
            return m;
        };

        PushNotification.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        PushNotification.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (!$util.isString(m.message))
                return "message: string expected";
            if (m.action != null && m.hasOwnProperty("action")) {
                if (!$util.isString(m.action))
                    return "action: string expected";
            }
            if (m.title != null && m.hasOwnProperty("title")) {
                if (!$util.isString(m.title))
                    return "title: string expected";
            }
            return null;
        };

        PushNotification.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.PushNotification)
                return d;
            var m = new $root.messages.PushNotification();
            if (d.message != null) {
                m.message = String(d.message);
            }
            if (d.action != null) {
                m.action = String(d.action);
            }
            if (d.title != null) {
                m.title = String(d.title);
            }
            return m;
        };

        PushNotification.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.message = "";
                d.action = "";
                d.title = "";
            }
            if (m.message != null && m.hasOwnProperty("message")) {
                d.message = m.message;
            }
            if (m.action != null && m.hasOwnProperty("action")) {
                d.action = m.action;
            }
            if (m.title != null && m.hasOwnProperty("title")) {
                d.title = m.title;
            }
            return d;
        };

        PushNotification.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PushNotification;
    })();

    messages.Envelope = (function() {

        function Envelope(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Envelope.prototype.version = 0;
        Envelope.prototype.service = "";
        Envelope.prototype.expiration = "";
        Envelope.prototype.messageType = "";
        Envelope.prototype.encryptedMessage = $util.newBuffer([]);
        Envelope.prototype.senderPublicKey = $util.newBuffer([]);
        Envelope.prototype.receiverPublicKey = $util.newBuffer([]);
        Envelope.prototype.identifier = "";
        Envelope.prototype.nonce = $util.newBuffer([]);
        Envelope.prototype.pushNotification = null;
        Envelope.prototype.signature = $util.newBuffer([]);

        Envelope.create = function create(properties) {
            return new Envelope(properties);
        };

        Envelope.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            w.uint32(8).int32(m.version);
            if (m.service != null && m.hasOwnProperty("service"))
                w.uint32(18).string(m.service);
            if (m.expiration != null && m.hasOwnProperty("expiration"))
                w.uint32(26).string(m.expiration);
            w.uint32(34).string(m.messageType);
            w.uint32(42).bytes(m.encryptedMessage);
            w.uint32(50).bytes(m.senderPublicKey);
            w.uint32(58).bytes(m.receiverPublicKey);
            w.uint32(66).string(m.identifier);
            w.uint32(74).bytes(m.nonce);
            if (m.pushNotification != null && m.hasOwnProperty("pushNotification"))
                $root.messages.PushNotification.encode(m.pushNotification, w.uint32(82).fork()).ldelim();
            w.uint32(90).bytes(m.signature);
            return w;
        };

        Envelope.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        Envelope.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.Envelope();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.version = r.int32();
                    break;
                case 2:
                    m.service = r.string();
                    break;
                case 3:
                    m.expiration = r.string();
                    break;
                case 4:
                    m.messageType = r.string();
                    break;
                case 5:
                    m.encryptedMessage = r.bytes();
                    break;
                case 6:
                    m.senderPublicKey = r.bytes();
                    break;
                case 7:
                    m.receiverPublicKey = r.bytes();
                    break;
                case 8:
                    m.identifier = r.string();
                    break;
                case 9:
                    m.nonce = r.bytes();
                    break;
                case 10:
                    m.pushNotification = $root.messages.PushNotification.decode(r, r.uint32());
                    break;
                case 11:
                    m.signature = r.bytes();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("version"))
                throw $util.ProtocolError("missing required 'version'", { instance: m });
            if (!m.hasOwnProperty("messageType"))
                throw $util.ProtocolError("missing required 'messageType'", { instance: m });
            if (!m.hasOwnProperty("encryptedMessage"))
                throw $util.ProtocolError("missing required 'encryptedMessage'", { instance: m });
            if (!m.hasOwnProperty("senderPublicKey"))
                throw $util.ProtocolError("missing required 'senderPublicKey'", { instance: m });
            if (!m.hasOwnProperty("receiverPublicKey"))
                throw $util.ProtocolError("missing required 'receiverPublicKey'", { instance: m });
            if (!m.hasOwnProperty("identifier"))
                throw $util.ProtocolError("missing required 'identifier'", { instance: m });
            if (!m.hasOwnProperty("nonce"))
                throw $util.ProtocolError("missing required 'nonce'", { instance: m });
            if (!m.hasOwnProperty("signature"))
                throw $util.ProtocolError("missing required 'signature'", { instance: m });
            return m;
        };

        Envelope.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        Envelope.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (!$util.isInteger(m.version))
                return "version: integer expected";
            if (m.service != null && m.hasOwnProperty("service")) {
                if (!$util.isString(m.service))
                    return "service: string expected";
            }
            if (m.expiration != null && m.hasOwnProperty("expiration")) {
                if (!$util.isString(m.expiration))
                    return "expiration: string expected";
            }
            if (!$util.isString(m.messageType))
                return "messageType: string expected";
            if (!(m.encryptedMessage && typeof m.encryptedMessage.length === "number" || $util.isString(m.encryptedMessage)))
                return "encryptedMessage: buffer expected";
            if (!(m.senderPublicKey && typeof m.senderPublicKey.length === "number" || $util.isString(m.senderPublicKey)))
                return "senderPublicKey: buffer expected";
            if (!(m.receiverPublicKey && typeof m.receiverPublicKey.length === "number" || $util.isString(m.receiverPublicKey)))
                return "receiverPublicKey: buffer expected";
            if (!$util.isString(m.identifier))
                return "identifier: string expected";
            if (!(m.nonce && typeof m.nonce.length === "number" || $util.isString(m.nonce)))
                return "nonce: buffer expected";
            if (m.pushNotification != null && m.hasOwnProperty("pushNotification")) {
                {
                    var e = $root.messages.PushNotification.verify(m.pushNotification);
                    if (e)
                        return "pushNotification." + e;
                }
            }
            if (!(m.signature && typeof m.signature.length === "number" || $util.isString(m.signature)))
                return "signature: buffer expected";
            return null;
        };

        Envelope.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.Envelope)
                return d;
            var m = new $root.messages.Envelope();
            if (d.version != null) {
                m.version = d.version | 0;
            }
            if (d.service != null) {
                m.service = String(d.service);
            }
            if (d.expiration != null) {
                m.expiration = String(d.expiration);
            }
            if (d.messageType != null) {
                m.messageType = String(d.messageType);
            }
            if (d.encryptedMessage != null) {
                if (typeof d.encryptedMessage === "string")
                    $util.base64.decode(d.encryptedMessage, m.encryptedMessage = $util.newBuffer($util.base64.length(d.encryptedMessage)), 0);
                else if (d.encryptedMessage.length)
                    m.encryptedMessage = d.encryptedMessage;
            }
            if (d.senderPublicKey != null) {
                if (typeof d.senderPublicKey === "string")
                    $util.base64.decode(d.senderPublicKey, m.senderPublicKey = $util.newBuffer($util.base64.length(d.senderPublicKey)), 0);
                else if (d.senderPublicKey.length)
                    m.senderPublicKey = d.senderPublicKey;
            }
            if (d.receiverPublicKey != null) {
                if (typeof d.receiverPublicKey === "string")
                    $util.base64.decode(d.receiverPublicKey, m.receiverPublicKey = $util.newBuffer($util.base64.length(d.receiverPublicKey)), 0);
                else if (d.receiverPublicKey.length)
                    m.receiverPublicKey = d.receiverPublicKey;
            }
            if (d.identifier != null) {
                m.identifier = String(d.identifier);
            }
            if (d.nonce != null) {
                if (typeof d.nonce === "string")
                    $util.base64.decode(d.nonce, m.nonce = $util.newBuffer($util.base64.length(d.nonce)), 0);
                else if (d.nonce.length)
                    m.nonce = d.nonce;
            }
            if (d.pushNotification != null) {
                if (typeof d.pushNotification !== "object")
                    throw TypeError(".messages.Envelope.pushNotification: object expected");
                m.pushNotification = $root.messages.PushNotification.fromObject(d.pushNotification);
            }
            if (d.signature != null) {
                if (typeof d.signature === "string")
                    $util.base64.decode(d.signature, m.signature = $util.newBuffer($util.base64.length(d.signature)), 0);
                else if (d.signature.length)
                    m.signature = d.signature;
            }
            return m;
        };

        Envelope.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.version = 0;
                d.service = "";
                d.expiration = "";
                d.messageType = "";
                if (o.bytes === String)
                    d.encryptedMessage = "";
                else {
                    d.encryptedMessage = [];
                    if (o.bytes !== Array)
                        d.encryptedMessage = $util.newBuffer(d.encryptedMessage);
                }
                if (o.bytes === String)
                    d.senderPublicKey = "";
                else {
                    d.senderPublicKey = [];
                    if (o.bytes !== Array)
                        d.senderPublicKey = $util.newBuffer(d.senderPublicKey);
                }
                if (o.bytes === String)
                    d.receiverPublicKey = "";
                else {
                    d.receiverPublicKey = [];
                    if (o.bytes !== Array)
                        d.receiverPublicKey = $util.newBuffer(d.receiverPublicKey);
                }
                d.identifier = "";
                if (o.bytes === String)
                    d.nonce = "";
                else {
                    d.nonce = [];
                    if (o.bytes !== Array)
                        d.nonce = $util.newBuffer(d.nonce);
                }
                d.pushNotification = null;
                if (o.bytes === String)
                    d.signature = "";
                else {
                    d.signature = [];
                    if (o.bytes !== Array)
                        d.signature = $util.newBuffer(d.signature);
                }
            }
            if (m.version != null && m.hasOwnProperty("version")) {
                d.version = m.version;
            }
            if (m.service != null && m.hasOwnProperty("service")) {
                d.service = m.service;
            }
            if (m.expiration != null && m.hasOwnProperty("expiration")) {
                d.expiration = m.expiration;
            }
            if (m.messageType != null && m.hasOwnProperty("messageType")) {
                d.messageType = m.messageType;
            }
            if (m.encryptedMessage != null && m.hasOwnProperty("encryptedMessage")) {
                d.encryptedMessage = o.bytes === String ? $util.base64.encode(m.encryptedMessage, 0, m.encryptedMessage.length) : o.bytes === Array ? Array.prototype.slice.call(m.encryptedMessage) : m.encryptedMessage;
            }
            if (m.senderPublicKey != null && m.hasOwnProperty("senderPublicKey")) {
                d.senderPublicKey = o.bytes === String ? $util.base64.encode(m.senderPublicKey, 0, m.senderPublicKey.length) : o.bytes === Array ? Array.prototype.slice.call(m.senderPublicKey) : m.senderPublicKey;
            }
            if (m.receiverPublicKey != null && m.hasOwnProperty("receiverPublicKey")) {
                d.receiverPublicKey = o.bytes === String ? $util.base64.encode(m.receiverPublicKey, 0, m.receiverPublicKey.length) : o.bytes === Array ? Array.prototype.slice.call(m.receiverPublicKey) : m.receiverPublicKey;
            }
            if (m.identifier != null && m.hasOwnProperty("identifier")) {
                d.identifier = m.identifier;
            }
            if (m.nonce != null && m.hasOwnProperty("nonce")) {
                d.nonce = o.bytes === String ? $util.base64.encode(m.nonce, 0, m.nonce.length) : o.bytes === Array ? Array.prototype.slice.call(m.nonce) : m.nonce;
            }
            if (m.pushNotification != null && m.hasOwnProperty("pushNotification")) {
                d.pushNotification = $root.messages.PushNotification.toObject(m.pushNotification, o);
            }
            if (m.signature != null && m.hasOwnProperty("signature")) {
                d.signature = o.bytes === String ? $util.base64.encode(m.signature, 0, m.signature.length) : o.bytes === Array ? Array.prototype.slice.call(m.signature) : m.signature;
            }
            return d;
        };

        Envelope.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Envelope;
    })();

    messages.Status = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "UNKNOWN"] = 0;
        values[valuesById[1] = "ACCEPTED"] = 1;
        values[valuesById[2] = "REJECTED"] = 2;
        return values;
    })();

    messages.Error = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "UNKNOWN_ERROR"] = 0;
        values[valuesById[1] = "USER_DENIED"] = 1;
        values[valuesById[2] = "SCOPE_UNKNOWN"] = 2;
        values[valuesById[3] = "REMOTE_ID_MISMATCH"] = 3;
        values[valuesById[4] = "NO_ADDRESS_FOUND"] = 4;
        values[valuesById[5] = "TRANSACTION_FAILED"] = 5;
        return values;
    })();

    messages.Link = (function() {

        function Link(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Link.prototype.publicKey = $util.newBuffer([]);
        Link.prototype.id = $util.newBuffer([]);
        Link.prototype.status = 0;
        Link.prototype.error = 0;

        Link.create = function create(properties) {
            return new Link(properties);
        };

        Link.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.publicKey != null && m.hasOwnProperty("publicKey"))
                w.uint32(10).bytes(m.publicKey);
            if (m.id != null && m.hasOwnProperty("id"))
                w.uint32(18).bytes(m.id);
            w.uint32(24).int32(m.status);
            if (m.error != null && m.hasOwnProperty("error"))
                w.uint32(32).int32(m.error);
            return w;
        };

        Link.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        Link.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.Link();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.publicKey = r.bytes();
                    break;
                case 2:
                    m.id = r.bytes();
                    break;
                case 3:
                    m.status = r.int32();
                    break;
                case 4:
                    m.error = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("status"))
                throw $util.ProtocolError("missing required 'status'", { instance: m });
            return m;
        };

        Link.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        Link.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (m.publicKey != null && m.hasOwnProperty("publicKey")) {
                if (!(m.publicKey && typeof m.publicKey.length === "number" || $util.isString(m.publicKey)))
                    return "publicKey: buffer expected";
            }
            if (m.id != null && m.hasOwnProperty("id")) {
                if (!(m.id && typeof m.id.length === "number" || $util.isString(m.id)))
                    return "id: buffer expected";
            }
            switch (m.status) {
            default:
                return "status: enum value expected";
            case 0:
            case 1:
            case 2:
                break;
            }
            if (m.error != null && m.hasOwnProperty("error")) {
                switch (m.error) {
                default:
                    return "error: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    break;
                }
            }
            return null;
        };

        Link.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.Link)
                return d;
            var m = new $root.messages.Link();
            if (d.publicKey != null) {
                if (typeof d.publicKey === "string")
                    $util.base64.decode(d.publicKey, m.publicKey = $util.newBuffer($util.base64.length(d.publicKey)), 0);
                else if (d.publicKey.length)
                    m.publicKey = d.publicKey;
            }
            if (d.id != null) {
                if (typeof d.id === "string")
                    $util.base64.decode(d.id, m.id = $util.newBuffer($util.base64.length(d.id)), 0);
                else if (d.id.length)
                    m.id = d.id;
            }
            switch (d.status) {
            case "UNKNOWN":
            case 0:
                m.status = 0;
                break;
            case "ACCEPTED":
            case 1:
                m.status = 1;
                break;
            case "REJECTED":
            case 2:
                m.status = 2;
                break;
            }
            switch (d.error) {
            case "UNKNOWN_ERROR":
            case 0:
                m.error = 0;
                break;
            case "USER_DENIED":
            case 1:
                m.error = 1;
                break;
            case "SCOPE_UNKNOWN":
            case 2:
                m.error = 2;
                break;
            case "REMOTE_ID_MISMATCH":
            case 3:
                m.error = 3;
                break;
            case "NO_ADDRESS_FOUND":
            case 4:
                m.error = 4;
                break;
            case "TRANSACTION_FAILED":
            case 5:
                m.error = 5;
                break;
            }
            return m;
        };

        Link.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                if (o.bytes === String)
                    d.publicKey = "";
                else {
                    d.publicKey = [];
                    if (o.bytes !== Array)
                        d.publicKey = $util.newBuffer(d.publicKey);
                }
                if (o.bytes === String)
                    d.id = "";
                else {
                    d.id = [];
                    if (o.bytes !== Array)
                        d.id = $util.newBuffer(d.id);
                }
                d.status = o.enums === String ? "UNKNOWN" : 0;
                d.error = o.enums === String ? "UNKNOWN_ERROR" : 0;
            }
            if (m.publicKey != null && m.hasOwnProperty("publicKey")) {
                d.publicKey = o.bytes === String ? $util.base64.encode(m.publicKey, 0, m.publicKey.length) : o.bytes === Array ? Array.prototype.slice.call(m.publicKey) : m.publicKey;
            }
            if (m.id != null && m.hasOwnProperty("id")) {
                d.id = o.bytes === String ? $util.base64.encode(m.id, 0, m.id.length) : o.bytes === Array ? Array.prototype.slice.call(m.id) : m.id;
            }
            if (m.status != null && m.hasOwnProperty("status")) {
                d.status = o.enums === String ? $root.messages.Status[m.status] : m.status;
            }
            if (m.error != null && m.hasOwnProperty("error")) {
                d.error = o.enums === String ? $root.messages.Error[m.error] : m.error;
            }
            return d;
        };

        Link.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Link;
    })();

    messages.Ping = (function() {

        function Ping(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Ping.prototype.ping = "";

        Ping.create = function create(properties) {
            return new Ping(properties);
        };

        Ping.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            w.uint32(10).string(m.ping);
            return w;
        };

        Ping.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        Ping.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.Ping();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.ping = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("ping"))
                throw $util.ProtocolError("missing required 'ping'", { instance: m });
            return m;
        };

        Ping.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        Ping.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (!$util.isString(m.ping))
                return "ping: string expected";
            return null;
        };

        Ping.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.Ping)
                return d;
            var m = new $root.messages.Ping();
            if (d.ping != null) {
                m.ping = String(d.ping);
            }
            return m;
        };

        Ping.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.ping = "";
            }
            if (m.ping != null && m.hasOwnProperty("ping")) {
                d.ping = m.ping;
            }
            return d;
        };

        Ping.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Ping;
    })();

    messages.Pong = (function() {

        function Pong(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        Pong.prototype.pong = "";

        Pong.create = function create(properties) {
            return new Pong(properties);
        };

        Pong.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            w.uint32(10).string(m.pong);
            return w;
        };

        Pong.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        Pong.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.Pong();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.pong = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("pong"))
                throw $util.ProtocolError("missing required 'pong'", { instance: m });
            return m;
        };

        Pong.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        Pong.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (!$util.isString(m.pong))
                return "pong: string expected";
            return null;
        };

        Pong.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.Pong)
                return d;
            var m = new $root.messages.Pong();
            if (d.pong != null) {
                m.pong = String(d.pong);
            }
            return m;
        };

        Pong.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.pong = "";
            }
            if (m.pong != null && m.hasOwnProperty("pong")) {
                d.pong = m.pong;
            }
            return d;
        };

        Pong.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Pong;
    })();

    messages.AccountRequest = (function() {

        function AccountRequest(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        AccountRequest.prototype.scope = "";

        AccountRequest.create = function create(properties) {
            return new AccountRequest(properties);
        };

        AccountRequest.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            w.uint32(10).string(m.scope);
            return w;
        };

        AccountRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        AccountRequest.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.AccountRequest();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.scope = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("scope"))
                throw $util.ProtocolError("missing required 'scope'", { instance: m });
            return m;
        };

        AccountRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        AccountRequest.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (!$util.isString(m.scope))
                return "scope: string expected";
            return null;
        };

        AccountRequest.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.AccountRequest)
                return d;
            var m = new $root.messages.AccountRequest();
            if (d.scope != null) {
                m.scope = String(d.scope);
            }
            return m;
        };

        AccountRequest.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.scope = "";
            }
            if (m.scope != null && m.hasOwnProperty("scope")) {
                d.scope = m.scope;
            }
            return d;
        };

        AccountRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return AccountRequest;
    })();

    messages.AccountResponse = (function() {

        function AccountResponse(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        AccountResponse.prototype.scope = "";
        AccountResponse.prototype.address = "";
        AccountResponse.prototype.status = 0;
        AccountResponse.prototype.error = 0;

        AccountResponse.create = function create(properties) {
            return new AccountResponse(properties);
        };

        AccountResponse.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.scope != null && m.hasOwnProperty("scope"))
                w.uint32(10).string(m.scope);
            if (m.address != null && m.hasOwnProperty("address"))
                w.uint32(18).string(m.address);
            w.uint32(24).int32(m.status);
            if (m.error != null && m.hasOwnProperty("error"))
                w.uint32(32).int32(m.error);
            return w;
        };

        AccountResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        AccountResponse.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.AccountResponse();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.scope = r.string();
                    break;
                case 2:
                    m.address = r.string();
                    break;
                case 3:
                    m.status = r.int32();
                    break;
                case 4:
                    m.error = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("status"))
                throw $util.ProtocolError("missing required 'status'", { instance: m });
            return m;
        };

        AccountResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        AccountResponse.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (m.scope != null && m.hasOwnProperty("scope")) {
                if (!$util.isString(m.scope))
                    return "scope: string expected";
            }
            if (m.address != null && m.hasOwnProperty("address")) {
                if (!$util.isString(m.address))
                    return "address: string expected";
            }
            switch (m.status) {
            default:
                return "status: enum value expected";
            case 0:
            case 1:
            case 2:
                break;
            }
            if (m.error != null && m.hasOwnProperty("error")) {
                switch (m.error) {
                default:
                    return "error: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    break;
                }
            }
            return null;
        };

        AccountResponse.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.AccountResponse)
                return d;
            var m = new $root.messages.AccountResponse();
            if (d.scope != null) {
                m.scope = String(d.scope);
            }
            if (d.address != null) {
                m.address = String(d.address);
            }
            switch (d.status) {
            case "UNKNOWN":
            case 0:
                m.status = 0;
                break;
            case "ACCEPTED":
            case 1:
                m.status = 1;
                break;
            case "REJECTED":
            case 2:
                m.status = 2;
                break;
            }
            switch (d.error) {
            case "UNKNOWN_ERROR":
            case 0:
                m.error = 0;
                break;
            case "USER_DENIED":
            case 1:
                m.error = 1;
                break;
            case "SCOPE_UNKNOWN":
            case 2:
                m.error = 2;
                break;
            case "REMOTE_ID_MISMATCH":
            case 3:
                m.error = 3;
                break;
            case "NO_ADDRESS_FOUND":
            case 4:
                m.error = 4;
                break;
            case "TRANSACTION_FAILED":
            case 5:
                m.error = 5;
                break;
            }
            return m;
        };

        AccountResponse.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.scope = "";
                d.address = "";
                d.status = o.enums === String ? "UNKNOWN" : 0;
                d.error = o.enums === String ? "UNKNOWN_ERROR" : 0;
            }
            if (m.scope != null && m.hasOwnProperty("scope")) {
                d.scope = m.scope;
            }
            if (m.address != null && m.hasOwnProperty("address")) {
                d.address = m.address;
            }
            if (m.status != null && m.hasOwnProperty("status")) {
                d.status = o.enums === String ? $root.messages.Status[m.status] : m.status;
            }
            if (m.error != null && m.hasOwnProperty("error")) {
                d.error = o.enums === String ? $root.messages.Error[m.error] : m.error;
            }
            return d;
        };

        AccountResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return AccountResponse;
    })();

    messages.PaymentRequest = (function() {

        function PaymentRequest(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        PaymentRequest.prototype.scope = "";
        PaymentRequest.prototype.network = "mainnet";
        PaymentRequest.prototype.address = "";
        PaymentRequest.prototype.amount = "";
        PaymentRequest.prototype.memo = "";
        PaymentRequest.prototype.transactionSize = "";
        PaymentRequest.prototype.transactionFee = "";

        PaymentRequest.create = function create(properties) {
            return new PaymentRequest(properties);
        };

        PaymentRequest.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            w.uint32(10).string(m.scope);
            if (m.network != null && m.hasOwnProperty("network"))
                w.uint32(18).string(m.network);
            w.uint32(26).string(m.address);
            w.uint32(34).string(m.amount);
            if (m.memo != null && m.hasOwnProperty("memo"))
                w.uint32(42).string(m.memo);
            if (m.transactionSize != null && m.hasOwnProperty("transactionSize"))
                w.uint32(50).string(m.transactionSize);
            if (m.transactionFee != null && m.hasOwnProperty("transactionFee"))
                w.uint32(58).string(m.transactionFee);
            return w;
        };

        PaymentRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        PaymentRequest.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.PaymentRequest();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.scope = r.string();
                    break;
                case 2:
                    m.network = r.string();
                    break;
                case 3:
                    m.address = r.string();
                    break;
                case 4:
                    m.amount = r.string();
                    break;
                case 5:
                    m.memo = r.string();
                    break;
                case 6:
                    m.transactionSize = r.string();
                    break;
                case 7:
                    m.transactionFee = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("scope"))
                throw $util.ProtocolError("missing required 'scope'", { instance: m });
            if (!m.hasOwnProperty("address"))
                throw $util.ProtocolError("missing required 'address'", { instance: m });
            if (!m.hasOwnProperty("amount"))
                throw $util.ProtocolError("missing required 'amount'", { instance: m });
            return m;
        };

        PaymentRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        PaymentRequest.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (!$util.isString(m.scope))
                return "scope: string expected";
            if (m.network != null && m.hasOwnProperty("network")) {
                if (!$util.isString(m.network))
                    return "network: string expected";
            }
            if (!$util.isString(m.address))
                return "address: string expected";
            if (!$util.isString(m.amount))
                return "amount: string expected";
            if (m.memo != null && m.hasOwnProperty("memo")) {
                if (!$util.isString(m.memo))
                    return "memo: string expected";
            }
            if (m.transactionSize != null && m.hasOwnProperty("transactionSize")) {
                if (!$util.isString(m.transactionSize))
                    return "transactionSize: string expected";
            }
            if (m.transactionFee != null && m.hasOwnProperty("transactionFee")) {
                if (!$util.isString(m.transactionFee))
                    return "transactionFee: string expected";
            }
            return null;
        };

        PaymentRequest.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.PaymentRequest)
                return d;
            var m = new $root.messages.PaymentRequest();
            if (d.scope != null) {
                m.scope = String(d.scope);
            }
            if (d.network != null) {
                m.network = String(d.network);
            }
            if (d.address != null) {
                m.address = String(d.address);
            }
            if (d.amount != null) {
                m.amount = String(d.amount);
            }
            if (d.memo != null) {
                m.memo = String(d.memo);
            }
            if (d.transactionSize != null) {
                m.transactionSize = String(d.transactionSize);
            }
            if (d.transactionFee != null) {
                m.transactionFee = String(d.transactionFee);
            }
            return m;
        };

        PaymentRequest.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.scope = "";
                d.network = "mainnet";
                d.address = "";
                d.amount = "";
                d.memo = "";
                d.transactionSize = "";
                d.transactionFee = "";
            }
            if (m.scope != null && m.hasOwnProperty("scope")) {
                d.scope = m.scope;
            }
            if (m.network != null && m.hasOwnProperty("network")) {
                d.network = m.network;
            }
            if (m.address != null && m.hasOwnProperty("address")) {
                d.address = m.address;
            }
            if (m.amount != null && m.hasOwnProperty("amount")) {
                d.amount = m.amount;
            }
            if (m.memo != null && m.hasOwnProperty("memo")) {
                d.memo = m.memo;
            }
            if (m.transactionSize != null && m.hasOwnProperty("transactionSize")) {
                d.transactionSize = m.transactionSize;
            }
            if (m.transactionFee != null && m.hasOwnProperty("transactionFee")) {
                d.transactionFee = m.transactionFee;
            }
            return d;
        };

        PaymentRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PaymentRequest;
    })();

    messages.PaymentResponse = (function() {

        function PaymentResponse(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        PaymentResponse.prototype.scope = "";
        PaymentResponse.prototype.status = 0;
        PaymentResponse.prototype.transactionId = "";
        PaymentResponse.prototype.error = 0;

        PaymentResponse.create = function create(properties) {
            return new PaymentResponse(properties);
        };

        PaymentResponse.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.scope != null && m.hasOwnProperty("scope"))
                w.uint32(10).string(m.scope);
            w.uint32(16).int32(m.status);
            if (m.transactionId != null && m.hasOwnProperty("transactionId"))
                w.uint32(26).string(m.transactionId);
            if (m.error != null && m.hasOwnProperty("error"))
                w.uint32(32).int32(m.error);
            return w;
        };

        PaymentResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        PaymentResponse.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.PaymentResponse();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.scope = r.string();
                    break;
                case 2:
                    m.status = r.int32();
                    break;
                case 3:
                    m.transactionId = r.string();
                    break;
                case 4:
                    m.error = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("status"))
                throw $util.ProtocolError("missing required 'status'", { instance: m });
            return m;
        };

        PaymentResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        PaymentResponse.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (m.scope != null && m.hasOwnProperty("scope")) {
                if (!$util.isString(m.scope))
                    return "scope: string expected";
            }
            switch (m.status) {
            default:
                return "status: enum value expected";
            case 0:
            case 1:
            case 2:
                break;
            }
            if (m.transactionId != null && m.hasOwnProperty("transactionId")) {
                if (!$util.isString(m.transactionId))
                    return "transactionId: string expected";
            }
            if (m.error != null && m.hasOwnProperty("error")) {
                switch (m.error) {
                default:
                    return "error: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    break;
                }
            }
            return null;
        };

        PaymentResponse.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.PaymentResponse)
                return d;
            var m = new $root.messages.PaymentResponse();
            if (d.scope != null) {
                m.scope = String(d.scope);
            }
            switch (d.status) {
            case "UNKNOWN":
            case 0:
                m.status = 0;
                break;
            case "ACCEPTED":
            case 1:
                m.status = 1;
                break;
            case "REJECTED":
            case 2:
                m.status = 2;
                break;
            }
            if (d.transactionId != null) {
                m.transactionId = String(d.transactionId);
            }
            switch (d.error) {
            case "UNKNOWN_ERROR":
            case 0:
                m.error = 0;
                break;
            case "USER_DENIED":
            case 1:
                m.error = 1;
                break;
            case "SCOPE_UNKNOWN":
            case 2:
                m.error = 2;
                break;
            case "REMOTE_ID_MISMATCH":
            case 3:
                m.error = 3;
                break;
            case "NO_ADDRESS_FOUND":
            case 4:
                m.error = 4;
                break;
            case "TRANSACTION_FAILED":
            case 5:
                m.error = 5;
                break;
            }
            return m;
        };

        PaymentResponse.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.scope = "";
                d.status = o.enums === String ? "UNKNOWN" : 0;
                d.transactionId = "";
                d.error = o.enums === String ? "UNKNOWN_ERROR" : 0;
            }
            if (m.scope != null && m.hasOwnProperty("scope")) {
                d.scope = m.scope;
            }
            if (m.status != null && m.hasOwnProperty("status")) {
                d.status = o.enums === String ? $root.messages.Status[m.status] : m.status;
            }
            if (m.transactionId != null && m.hasOwnProperty("transactionId")) {
                d.transactionId = m.transactionId;
            }
            if (m.error != null && m.hasOwnProperty("error")) {
                d.error = o.enums === String ? $root.messages.Error[m.error] : m.error;
            }
            return d;
        };

        PaymentResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PaymentResponse;
    })();

    messages.CallRequest = (function() {

        function CallRequest(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        CallRequest.prototype.scope = "";
        CallRequest.prototype.network = "mainnet";
        CallRequest.prototype.address = "";
        CallRequest.prototype.abi = "";
        CallRequest.prototype.amount = "";
        CallRequest.prototype.memo = "";
        CallRequest.prototype.transactionSize = "";
        CallRequest.prototype.transactionFee = "";

        CallRequest.create = function create(properties) {
            return new CallRequest(properties);
        };

        CallRequest.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            w.uint32(10).string(m.scope);
            if (m.network != null && m.hasOwnProperty("network"))
                w.uint32(18).string(m.network);
            w.uint32(26).string(m.address);
            w.uint32(34).string(m.abi);
            w.uint32(42).string(m.amount);
            if (m.memo != null && m.hasOwnProperty("memo"))
                w.uint32(50).string(m.memo);
            if (m.transactionSize != null && m.hasOwnProperty("transactionSize"))
                w.uint32(58).string(m.transactionSize);
            if (m.transactionFee != null && m.hasOwnProperty("transactionFee"))
                w.uint32(66).string(m.transactionFee);
            return w;
        };

        CallRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        CallRequest.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.CallRequest();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.scope = r.string();
                    break;
                case 2:
                    m.network = r.string();
                    break;
                case 3:
                    m.address = r.string();
                    break;
                case 4:
                    m.abi = r.string();
                    break;
                case 5:
                    m.amount = r.string();
                    break;
                case 6:
                    m.memo = r.string();
                    break;
                case 7:
                    m.transactionSize = r.string();
                    break;
                case 8:
                    m.transactionFee = r.string();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("scope"))
                throw $util.ProtocolError("missing required 'scope'", { instance: m });
            if (!m.hasOwnProperty("address"))
                throw $util.ProtocolError("missing required 'address'", { instance: m });
            if (!m.hasOwnProperty("abi"))
                throw $util.ProtocolError("missing required 'abi'", { instance: m });
            if (!m.hasOwnProperty("amount"))
                throw $util.ProtocolError("missing required 'amount'", { instance: m });
            return m;
        };

        CallRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        CallRequest.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (!$util.isString(m.scope))
                return "scope: string expected";
            if (m.network != null && m.hasOwnProperty("network")) {
                if (!$util.isString(m.network))
                    return "network: string expected";
            }
            if (!$util.isString(m.address))
                return "address: string expected";
            if (!$util.isString(m.abi))
                return "abi: string expected";
            if (!$util.isString(m.amount))
                return "amount: string expected";
            if (m.memo != null && m.hasOwnProperty("memo")) {
                if (!$util.isString(m.memo))
                    return "memo: string expected";
            }
            if (m.transactionSize != null && m.hasOwnProperty("transactionSize")) {
                if (!$util.isString(m.transactionSize))
                    return "transactionSize: string expected";
            }
            if (m.transactionFee != null && m.hasOwnProperty("transactionFee")) {
                if (!$util.isString(m.transactionFee))
                    return "transactionFee: string expected";
            }
            return null;
        };

        CallRequest.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.CallRequest)
                return d;
            var m = new $root.messages.CallRequest();
            if (d.scope != null) {
                m.scope = String(d.scope);
            }
            if (d.network != null) {
                m.network = String(d.network);
            }
            if (d.address != null) {
                m.address = String(d.address);
            }
            if (d.abi != null) {
                m.abi = String(d.abi);
            }
            if (d.amount != null) {
                m.amount = String(d.amount);
            }
            if (d.memo != null) {
                m.memo = String(d.memo);
            }
            if (d.transactionSize != null) {
                m.transactionSize = String(d.transactionSize);
            }
            if (d.transactionFee != null) {
                m.transactionFee = String(d.transactionFee);
            }
            return m;
        };

        CallRequest.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.scope = "";
                d.network = "mainnet";
                d.address = "";
                d.abi = "";
                d.amount = "";
                d.memo = "";
                d.transactionSize = "";
                d.transactionFee = "";
            }
            if (m.scope != null && m.hasOwnProperty("scope")) {
                d.scope = m.scope;
            }
            if (m.network != null && m.hasOwnProperty("network")) {
                d.network = m.network;
            }
            if (m.address != null && m.hasOwnProperty("address")) {
                d.address = m.address;
            }
            if (m.abi != null && m.hasOwnProperty("abi")) {
                d.abi = m.abi;
            }
            if (m.amount != null && m.hasOwnProperty("amount")) {
                d.amount = m.amount;
            }
            if (m.memo != null && m.hasOwnProperty("memo")) {
                d.memo = m.memo;
            }
            if (m.transactionSize != null && m.hasOwnProperty("transactionSize")) {
                d.transactionSize = m.transactionSize;
            }
            if (m.transactionFee != null && m.hasOwnProperty("transactionFee")) {
                d.transactionFee = m.transactionFee;
            }
            return d;
        };

        CallRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return CallRequest;
    })();

    messages.CallResponse = (function() {

        function CallResponse(p) {
            if (p)
                for (var ks = Object.keys(p), i = 0; i < ks.length; ++i)
                    if (p[ks[i]] != null)
                        this[ks[i]] = p[ks[i]];
        }

        CallResponse.prototype.scope = "";
        CallResponse.prototype.status = 0;
        CallResponse.prototype.transactionId = "";
        CallResponse.prototype.error = 0;

        CallResponse.create = function create(properties) {
            return new CallResponse(properties);
        };

        CallResponse.encode = function encode(m, w) {
            if (!w)
                w = $Writer.create();
            if (m.scope != null && m.hasOwnProperty("scope"))
                w.uint32(10).string(m.scope);
            w.uint32(16).int32(m.status);
            if (m.transactionId != null && m.hasOwnProperty("transactionId"))
                w.uint32(26).string(m.transactionId);
            if (m.error != null && m.hasOwnProperty("error"))
                w.uint32(32).int32(m.error);
            return w;
        };

        CallResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        CallResponse.decode = function decode(r, l) {
            if (!(r instanceof $Reader))
                r = $Reader.create(r);
            var c = l === undefined ? r.len : r.pos + l, m = new $root.messages.CallResponse();
            while (r.pos < c) {
                var t = r.uint32();
                switch (t >>> 3) {
                case 1:
                    m.scope = r.string();
                    break;
                case 2:
                    m.status = r.int32();
                    break;
                case 3:
                    m.transactionId = r.string();
                    break;
                case 4:
                    m.error = r.int32();
                    break;
                default:
                    r.skipType(t & 7);
                    break;
                }
            }
            if (!m.hasOwnProperty("status"))
                throw $util.ProtocolError("missing required 'status'", { instance: m });
            return m;
        };

        CallResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        CallResponse.verify = function verify(m) {
            if (typeof m !== "object" || m === null)
                return "object expected";
            if (m.scope != null && m.hasOwnProperty("scope")) {
                if (!$util.isString(m.scope))
                    return "scope: string expected";
            }
            switch (m.status) {
            default:
                return "status: enum value expected";
            case 0:
            case 1:
            case 2:
                break;
            }
            if (m.transactionId != null && m.hasOwnProperty("transactionId")) {
                if (!$util.isString(m.transactionId))
                    return "transactionId: string expected";
            }
            if (m.error != null && m.hasOwnProperty("error")) {
                switch (m.error) {
                default:
                    return "error: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    break;
                }
            }
            return null;
        };

        CallResponse.fromObject = function fromObject(d) {
            if (d instanceof $root.messages.CallResponse)
                return d;
            var m = new $root.messages.CallResponse();
            if (d.scope != null) {
                m.scope = String(d.scope);
            }
            switch (d.status) {
            case "UNKNOWN":
            case 0:
                m.status = 0;
                break;
            case "ACCEPTED":
            case 1:
                m.status = 1;
                break;
            case "REJECTED":
            case 2:
                m.status = 2;
                break;
            }
            if (d.transactionId != null) {
                m.transactionId = String(d.transactionId);
            }
            switch (d.error) {
            case "UNKNOWN_ERROR":
            case 0:
                m.error = 0;
                break;
            case "USER_DENIED":
            case 1:
                m.error = 1;
                break;
            case "SCOPE_UNKNOWN":
            case 2:
                m.error = 2;
                break;
            case "REMOTE_ID_MISMATCH":
            case 3:
                m.error = 3;
                break;
            case "NO_ADDRESS_FOUND":
            case 4:
                m.error = 4;
                break;
            case "TRANSACTION_FAILED":
            case 5:
                m.error = 5;
                break;
            }
            return m;
        };

        CallResponse.toObject = function toObject(m, o) {
            if (!o)
                o = {};
            var d = {};
            if (o.defaults) {
                d.scope = "";
                d.status = o.enums === String ? "UNKNOWN" : 0;
                d.transactionId = "";
                d.error = o.enums === String ? "UNKNOWN_ERROR" : 0;
            }
            if (m.scope != null && m.hasOwnProperty("scope")) {
                d.scope = m.scope;
            }
            if (m.status != null && m.hasOwnProperty("status")) {
                d.status = o.enums === String ? $root.messages.Status[m.status] : m.status;
            }
            if (m.transactionId != null && m.hasOwnProperty("transactionId")) {
                d.transactionId = m.transactionId;
            }
            if (m.error != null && m.hasOwnProperty("error")) {
                d.error = o.enums === String ? $root.messages.Error[m.error] : m.error;
            }
            return d;
        };

        CallResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return CallResponse;
    })();

    return messages;
})();

export { $root as default };
