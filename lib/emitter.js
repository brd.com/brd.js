function Emitter() {
  var e = Object.create(emitter);
  e.events = {};
  return e;
}

var emitter = {};

emitter.on = function(type, handler) {
  if (this.events.hasOwnProperty(type)) {
    this.events[type].push(handler);
  } else {
    this.events[type] = [handler];
  }
  return this;
};

emitter.off = function(type, handler) {
  if (arguments.length === 0) {
    return this._offAll();
  }
  if (handler === undefined) {
    return this._offByType(type);
  }
  return this._offByHandler(type, handler);
};

emitter.emit = function(event, ...args) {
  return this._dispatch(event, args);
};

emitter._dispatch = function(event, args) {
  args = args || [];

  var handlers = this.events[event] || [];
  handlers.forEach(handler => handler.apply(null, args));
  return this;
};

emitter._offByHandler = function(type, handler) {
  if (!this.events.hasOwnProperty(type)) return;
  var i = this.events[type].indexOf(handler);
  if (i > -1) {
    this.events[type].splice(i, 1);
  }
  return this;
};

emitter._offByType = function(type) {
  if (this.events.hasOwnProperty(type)) {
    delete this.events[type];
  }
  return this;
};

emitter._offAll = function() {
  this.events = {};
  return this;
};

Emitter.mixin = function(obj){
  var emitter = new Emitter();
  ['on','off','emit'].map(function(name){
    obj[name] = function(){
      return emitter[name].apply(emitter, arguments);
    };
  });
};

export default Emitter;
