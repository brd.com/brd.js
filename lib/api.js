import { prepareForOnceReady, onceReady } from './utils'
import { signHttp } from './api-authentication';
import uuid from 'uuid/v4';

const PrivateKey = require('bitcore-lib/privatekey')
const base58 = require('bitcore-lib/encoding/base58')

export const STAGE_PROXY = 'https://stage2.breadwallet.com';
export const PROD_PROXY = 'https://stage2.breadwallet.com';

export const STAGE_API = 'https://stage2.breadwallet.com';
export const PROD_API = 'https://stage2.breadwallet.com';

const DEFAULTS = {
  // use for API overrides, will use stage/prod
  api: null,

  // Defaults to production server:
  staging: false,

  // force use of proxy (true/false), null uses proxy in browser environments,
  // direct in server environments
  proxy: null,

  // token and privateKey are required in server-side environments, but in
  // client-side  environments, they will be automatically generated and stored
  // in localStorage for you.

  // token if you want to persist yourself:
  token: null,

  // private key & deviceID if you want to persist yourself:
  privateKey: null,
  deviceID: null,

  // A fetch polyfill if desired:
  fetch: null,

  // persistence for keys, deviceID, etc:
  persistence: (typeof localStorage!='undefined')?localStorage:null,
}

function API(options) {
  this.options = Object.assign({},DEFAULTS,options);
  prepareForOnceReady.call(this,async function() {
    // we need to first ensure we have a private key and a token:
    ensureAPIBase.call(this);
    
    await initializePrivateKey.call(this);
    await ensureDeviceID.call(this)
    
    await ensureToken.call(this);
  });
}

export function getItem(key) {
  if(this.options.persistence) {
    var val = this.options.persistence.getItem(key);
    if(val) {
      try {
        return JSON.parse(val);
      } catch(x) {}
    }

    return val;
  }

  return null;
}

export function setItem(key,value) {
  if(this.options.persistence) {
    return this.options.persistence.setItem(key,JSON.stringify(value));
  }

  return null;
}

export function removeItem(key) {
  if(this.options.persistence) {
    return this.options.persistence.removeItem(key);
  }

  return null;
}


function ensureAPIBase() {
  var api;
  var proxy = this.options.proxy || (typeof window != 'undefined');
  this.proxy = proxy;

  if(!(api = this.options.api)) {

    var staging = this.options.staging;

    if(proxy) {
      if(staging) api = STAGE_PROXY;
      else api = PROD_PROXY;
    } else {
      if(staging) api = STAGE_API;
      else api = PROD_API;
    }
  }

  this.api = api;
}

async function initializePrivateKey() {
  var privateKey;

  if(!(privateKey = this.options.privateKey)) {

    privateKey = privateKey || await getItem.call(this,'brd-pk');

    if(!privateKey) {
      privateKey = new PrivateKey();
      await setItem.call(this,'brd-pk',privateKey.toString());
    }
  }

  this.privateKey = new PrivateKey(privateKey);
}

async function ensureDeviceID() {
  var deviceID;

  if(!(deviceID = this.options.deviceID)) {
    deviceID = deviceID || await getItem.call(this,'brd-device');

    if(!deviceID) {
      deviceID = uuid();
      await setItem.call(this,'brd-device',deviceID);
    }
  }

  this.deviceID = deviceID;
}

async function ensureToken() {
  var token;

  if(!(token = this.options.token)) {
    token = token || await getItem.call(this,'brd-token');

    var { result , error } = await internalFetch.call(this,'/token',{
      method: 'POST',
      body: { pubKey: base58.encode(this.privateKey.toPublicKey().toBuffer()),
        deviceID: this.deviceID }
    });

    if(error) {
      throw new Error(error.statusText);
    }

    token = result.token;
    await setItem.call(this,'brd-token',token);
  }

  this.token = token;
}

async function internalFetch(path,options) {
  var body = options.body;
  var headers = { accept: 'application/json' };

  if(options && options.headers) {
    for(var k in options.headers) {
      headers[k.toLowerCase()] = options.headers[k];
    }
  }

  if(!('content-type' in headers) && typeof body == 'object' ) {
    headers['content-type'] = 'application/json';
    body = JSON.stringify(body);
  }

  if(this.proxy) {
    // date header gets messed up when proxying, switch it to x-date:
    var date = headers.date;
    delete headers.date;
    headers['x-date'] = date;
  }

  var hash = Object.assign(
    {},options,
    body?{body:body}:{},
    headers?{headers:headers}:{},
  );

  // console.log("Fetching ",this.api+path);
  // console.log(hash);

  var res = await (this.options.fetch||fetch)(this.api + path,hash);

  if(res.status != 200) return { error: res };

  var json = await res.json();

  return { result: json };
}


API.prototype.destroy = async function() {
  this._latch.reject(new Error('destroyed api object'));
  
  // if ready is false, we won't call through:
  this._ready = false;

  // create a stub latch that always rejects everything:
  this._latch = { promise: () => new Promise((res,rej) => rej(new Error('destroyed api object'))) };

  var options = this.options;
  
  // zero out everything else:
  this.token = null;
  this.deviceID = null;
  this.privateKey = null;
  this.proxy = null;
  this.api = null;
  this.options = null;

    // remove these from our persistence layer if it exists:
  await removeItem.call({options},'brd-pk');
  await removeItem.call({options},'brd-token');
  await removeItem.call({options},'brd-device');
  await removeItem.call({options},'brd-eme');

};

Object.assign(API.prototype,onceReady({
  fetch: function(path,options={}) {
    var headers = {};
    var body = options.body;

    if(options && options.headers) {
      for(var k in options.headers) {
        headers[k.toLowerCase()] = options.headers[k];
      }
    }

    if(!('content-type' in headers) && typeof body == 'object' ) {
      headers['content-type'] = 'application/json';
      body = JSON.stringify(body);
    }

    headers.date = (new Date()).toUTCString();
    var hash = Object.assign(
      {path:path},
      options,
      body?{body:body}:{},
      headers?{headers:headers}:{headers:{}},
    );

    hash.headers.authorization = signHttp(this.privateKey,this.token,hash);
        
    return internalFetch.call(this,path,hash);
  },
}));

export default API;
