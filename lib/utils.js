export function PromiseLatch() {
  this._res = [];
  this._rej = [];
}

PromiseLatch.prototype = {
  promise: function() {
    return new Promise((res,rej) => {
      this._res.push(res);
      this._rej.push(rej);
    });
  },
  resolve: function(...args) {
    if (this._res) {
      var call = this._res;
      this._rej = [];
      this._res = [];

      call.forEach((c) => c(...args));
    }
  },
  reject: function() {
    var args = Array.prototype.slice.call(arguments);
    if (this._rej) {
      var call = this._rej;
      this._rej = [];
      this._res = [];

      call.forEach((c) => c.apply(this,args));
    }
  },
}

export function onceReady(fn) {
  if(typeof fn == 'object') {
    var r = {};
    for(var k in fn) {
      r[k] = onceReady(fn[k]);
    }
    r.ready = function() {
      if(this._ready) return Promise.resolve(true);
      
      return this._latch.promise();
    }
    return r;
  }

  if(typeof fn == 'function') {
    return function() {
      var args = Array.prototype.slice.call(arguments)
      // if we're ready, call through to the function:
      if(this._ready) return fn.apply(this,args);
      // if we're not ready, wait until we're ready, and then call through.  If
      // "readiness" fails, return a failing promise:
      throw new Error('not ready - use the promise that the ready function returns');
    }
  }

  return fn;
}

export async function prepareForOnceReady(setupCallback) {
  this._ready = false;
  this._latch = new PromiseLatch();
  
  try {
    await (setupCallback ? setupCallback.call(this) : this.asyncSetup());
  } catch(x) {
    console.error("Error setting up: ",x);
    this._ready = false;
    this._error = x;
    this._latch.reject(x);
    throw x;
  }

  this._ready = true;
  this._latch.resolve(true);
}

export function pause(duration) {
  return new Promise(res => setTimeout(res,duration));
}
                    
