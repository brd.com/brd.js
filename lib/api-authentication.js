var PrivateKey = require('bitcore-lib/privatekey');
var Hash = require('bitcore-lib/crypto/hash');
var base58 = require('bitcore-lib/encoding/base58');
var ECDSA = require('bitcore-lib/crypto/ecdsa');
import { Buffer } from 'buffer';

function assertToken(token) {
  if(typeof token != 'string') throw "Token must be a string";
  if(!token.match(/^[a-zA-Z0-9]{128}$/)) throw "Token must be a string of 128 alphanumeric characters";
}

var ALLOWED_METHODS = ['GET','POST','PATCH','PUT','DELETE'];

function sign(buffer,privateKey) {
  var hashbuf = Hash.sha256sha256(buffer)
  var ecdsa = new ECDSA();

  ecdsa.hashbuf = hashbuf;
  ecdsa.privkey = privateKey;
  ecdsa.pubkey = privateKey.toPublicKey();
  ecdsa.signRandomK();
  ecdsa.calci();

  return ecdsa.sig;
}

function pathFor(url) {
  var m = url.match(/^(?:.*?:\/\/.*?)?(\/.*?)$/);
  return m && m[1];
}

export function signHttp(privateKey,token,hash) {
  var pk = new PrivateKey(privateKey);
  assertToken(token);

  var path = hash.path || pathFor(hash.url || hash.uri);
  var method = (hash.method || 'GET').toUpperCase();
  var headers = {};
  var body = hash.body || null;

  // ensure headers are lower-case so we can reliably get to them:
  for(var k in hash.headers) {
    headers[k.toLowerCase()] = hash.headers[k];
  }

  if(!path || typeof path != 'string') throw new Error("URL or URI must be a string");
  if(typeof method != 'string') throw new Error("Method must be a string");
  if(!ALLOWED_METHODS.includes(method)) throw new Error("Method must be an HTTP method");

  // best way I know of to figure out if it's a valid date:
  if(isNaN((new Date(headers.date)).getTime())) throw new Error("Must specify a Date header");

  let bodySHA256 = '';
  if (!!body) {
    var sha = Hash.sha256(Buffer.from(body));
    bodySHA256 = base58.encode(sha);
  }

  var parts = [
    method,
    bodySHA256,
    headers['content-type'] || '',
    headers['date'],
    path,
  ];

  var signature = sign(Buffer.from(parts.join('\n')),privateKey);

  return `bread ${token}:${base58.encode(signature.toCompact())}`
}
