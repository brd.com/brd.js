import API from './api';
import EME from './eme';
import {signHttp} from './api-authentication';

import { onceReady, prepareForOnceReady } from './utils';

const DEFAULTS = {

}

function BRD(options) {
  if(!options) {
    console.error("BRD requires an options hash with a service identifier: e.g. new BRD({service: 'test'}), get one from BRD");
    throw new Error("BRD requires an options hash with at least a service identifier");
  }
  if(!options.service) {
    console.error("BRD requires a service identifier: e.g. new BRD({service: 'test'}), get one from BRD");
    throw new Error("BRD requires a service identifier");
  }
  this.options = Object.assign({},DEFAULTS,options)

  this.api = new API(this.options);
  this.eme = new EME(this.api,this.options.service);
  
  prepareForOnceReady.call(this,async function() {
    await this.api.ready();

    // in case persistence is async:
    await this.eme.ready();

    // make sure we are up-to-date with the latest inbox messages:
    await this.eme.fetch();
  });
}

BRD.signHttp = signHttp;
BRD.API = API;
BRD.EME = EME;

function errorFor(index) {
  for(var err in EME.proto.Error) {
    if(EME.proto.Error[err] == index)
      return err;
  }
  
  return 'unknown';
}

Object.assign(BRD.prototype,onceReady({
  awaitMessage(filter,options) {
    return new Promise((res,rej) => {

      const done = (fn,object) => {
        this.eme.stopPolling();
        fn(object);
      }

      if(options && options.cancel) options.cancel = () => done(rej,new Error('canceled'));
      
      this.eme.on('message',(message,envelope,entry) => {
        try {
          if(filter(message,envelope,entry)) done(res,entry);
        } catch(x) {
          done(rej,x);
        }
      });
      
      this.eme.startPolling();
    });
  },

  awaitResponse(id,options) {
    return this.awaitMessage((message,envelope,entry) => envelope.identifier == id,options);
  },
  
  async destroy() {
    await this.eme.destroy();
    await this.api.destroy();
  },
  
  async link(relink=false,ref) {
    // the link function wants to link with another party.  There are
    // three ingoing states:
    //
    // 1. we're already fully linked (inbound, ACKed LINK message)
    // 2. we're halfway-through linking (inbound, un-ACKed LINK message)
    // 3. we're not linked at all (no inbound LINK message)
    //
    // if relink is TRUE (it defaults to false), case 1 will basically
    // ignore ACKed LINK messages, and we'll wait for a new one.

    var eme = this.eme;
    
    var links = eme.entriesOfType(
      'LINK',
      (entry) => entry.envelope.message.status == eme.proto.Status.ACCEPTED
    ).reverse();

    var entry;
    
    // check for case 1:
    if(entry = links.find(entry => entry.acknowledged)) {
      if(!relink) return entry.envelope
    }

    if(!(entry = links.find(entry => !entry.acknowledged))) {
      // we cannot find an unacknowledged, ACCEPTED, 'LINK' entry,
      // wait for one:
      entry = await this.awaitMessage((message,envelope,entry) => (
        envelope.messageType == 'LINK' &&
          entry.acknowledged == false && 
          message.status == eme.proto.Status.ACCEPTED
      ),ref);
    }

    // now, entry is an unacknoledged, ACCEPTED LINK message, we
    // should send an ACCEPTED response:

    await eme.send(
      entry.envelope.message.publicKey,
      'LINK',
      { status: eme.proto.Status.ACCEPTED,
        publicKey: eme.api.privateKey.toPublicKey().toBuffer(),
        id: eme.Buffer.from(this.options.id || this.api.deviceID),
      }
    );

    await eme.ack(entry);

    return entry.envelope;
  },

  // request an address from the linked party.  if they reject your
  // request, will return null.  otherwise will wait for an answer.

  async requestAddress(scope='ETH',force=false) {
    var pubkey = this.eme.linkedPubkey();
    if(!pubkey) {
      console.error("must be linked to request address");
      throw new Error("Must be linked to request address");
    }

    var entry = this.eme.entriesOfType('ACCOUNT_RESPONSE').reverse()[0];

    if(!entry || force) {
      var id = await this.eme.send(pubkey,'ACCOUNT_REQUEST',{ scope });
      
      var entry = await this.awaitResponse(id);
    }

    if(!entry.acknowledged) {
      this.eme.ack(entry);
    }

    if(entry.envelope.message.status != this.eme.proto.Status.ACCEPTED) {
      console.error("Error: ",errorFor(entry.envelope.message.error));
      return null;
    }

    return entry.envelope.message.address;
  },

  
  async paymentRequest(scope,address,amount,options={ network: 'mainnet', memo: null, transactionSize: null, transactionFee: null, pushNotification: null }) {
    var pubkey = this.eme.linkedPubkey();
    if(!pubkey) {
      console.error("must be linked to request address");
      throw new Error("Must be linked to request address");
    }

    var hash = {
      ...options,
      scope , address, amount,
      network: options.network || 'mainnet'
    };

    var id = await this.eme.send(pubkey,'PAYMENT_REQUEST',hash,{
      pushNotification: options.pushNotification
    });

    var entry = await this.awaitResponse(id);

    this.eme.ack(entry);

    return entry;
  },

  async callRequest(scope,address,amount,abi,options={ network: 'mainnet', memo: null, transactionSize: null, transactionFee: null, pushNotification: null }) {
    var pubkey = this.eme.linkedPubkey();
    if(!pubkey) {
      console.error("must be linked to request address");
      throw new Error("Must be linked to request address");
    }

    var hash = {
      ...options,
      scope , address, amount, abi,
      network: options.network || 'mainnet'
    };

    var id = await this.eme.send(pubkey,'CALL_REQUEST',hash,{ pushNotification: options.pushNotification });

    var entry = await this.awaitResponse(id);

    this.eme.ack(entry);

    return entry;
  },  
  
}));

export default BRD;
