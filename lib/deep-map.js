export const DELETE = {};
const isArrayLike = (a) => a instanceof Array;
const isObjectLike = (a) => typeof a == 'object' && !isArrayLike(a);

function deepMapArray(callback,value,key=null,path='') {
  var returning = [];

  for(var i = 0 ; i < value.length ; i++) {
    var v = value[i];
    var r = deepMap(callback,v,i,path+'.'+i);
    if(r === DELETE) continue;
    else if(typeof r == 'undefined') {
      returning.push(v);
    } else {
      returning.push(r);
    }
  }

  return returning;
}

function deepMapObject(callback,value,key=null,path='') {
  var returning = {};

  for(var k in value) {
    if(!value.hasOwnProperty(k)) {
      continue;
    }

    var v = value[k];
    var r = deepMap(callback,v,k,path+'.'+k);
    if(r === DELETE) continue;
    else if(typeof r == 'undefined') {
      returning[k] = v;
    } else {
      returning[k] = r;
    }
  }

  return returning;
}

export default function deepMap(callback,value,in_key,in_path) {
  if(arguments.length == 0) {
    throw new Error("Must pass something to deepMap");
  }

  var key = arguments.length >= 3 ? in_key : null;
  var path = arguments.length >= 4 ? in_path : null;

  function inner(callback,value,key=null,path='') {
    var r = callback(value,key,path);
    // return undefined = keep going
    if(r === DELETE) return DELETE;
    if(typeof r == 'undefined') {
      // default behavior:
      if(isArrayLike(value)) {
        return deepMapArray(callback,value,key,path);
      } else if(isObjectLike(value)) {
        return deepMapObject(callback,value,key,path);
      } else {
        return value;
      }
    }
    return r;
  }

  // simple currying:
  if(arguments.length == 1) {
    return function(...rest) {
      return inner(callback,...rest);
    }
  }

  return inner(callback,value,key,path);
}
