# BRD.js
[![pipeline status](https://gitlab.com/brd.com/brd.js/badges/master/pipeline.svg)](https://gitlab.com/brd.com/brd.js/commits/master)
[Fork me on GitLab](https://gitlab.com/brd.com/brd.js)

Are you looking to add some secure crypto purchasing power to your
website or server? If so, then you've come to the right place!

BRD.js is a package for easy communication between the BRD app and
your Javascript (client or server). Authentication, sessions, and
client-side encryption are all handled in the library itself. It's
designed to be flexible; all hard requirements are inline and stripped
down for a small build, but are designed to be easily replaced with
somewhat standard components. You can use it in one of two ways,
directly included as a javascript file into HTML for simple,
bullet-proof integration, or built using Webpack into a more
customized solution.

## BRD api:

[Simplified API](/simple)

#### Simple Example Code:
```javascript
var api = new BRD.API( { api: 'https://stage2.breadwallet.com' });
var eme = new BRD.EME(api,'PWB');

// display as QR code:
var qr = eme.qrLinkUrl();

// or display as a deep link into BRD:
var deepLink = eme.deepLinkUrl();

eme.on('link',function(message,envelope,entry) {
  // we are linked!  pubkey we linked with is in message.publicKey
  // maybe send an account request or payment request.

  // or, stop polling and do something else:
  eme.stopPolling();
}) 

eme.startPolling();

```

