import Vue from 'vue';

const requireComponent = require.context('../components',true,/^[^_].*?\.(vue|js)$/)
requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName);
  const componentName = fileName.replace(/^\.\//, '').replace(/\.\w+$/, '').replace(/ui\//g,'');
  
  Vue.component(componentName,componentConfig.default || componentConfig);
});
