import Vue from 'vue';

if(process.client) {
  var BRD = require('../lib/brd').default;


  var api = DEFAULT_API;
  
  if(typeof localStorage != 'undefined') {
    api = localStorage.getItem('brd-api') || DEFAULT_API;

    var token = JSON.parse(localStorage.getItem('brd-token-'+api)||"null");
    var privateKey = JSON.parse(localStorage.getItem('brd-pk')||"null");
    var deviceID = JSON.parse(localStorage.getItem('brd-device')||"null");
  }

  var brd = new BRD({ service: 'brd-js-tools', api, token, privateKey, deviceID, persistence: null });

  brd.ready().then(() => {
    localStorage.setItem('brd-token-'+api,JSON.stringify(brd.api.token));
    localStorage.setItem('brd-pk',JSON.stringify(brd.api.privateKey));
    localStorage.setItem('brd-device',JSON.stringify(brd.api.deviceID));
  });
  
  console.log("Hello, developer!");
  console.log("BRD constructor and brd instance is available globally!");
  console.log("e.g. brd.eme.inbox");
  
  window.BRD = BRD;
  window.brd = brd;

  
  function proxy(object) {
    var r = {};
    for(var k in object) {
      var v = object[k];
      if(typeof v != 'function') continue;

      r[k] = v.bind(object);
    }
    return r;
  }

  Vue.prototype.$brd = new Vue({
    data() {
      var data = {};
      for(var k in brd) {
        var v = brd[k];
        if(typeof v == 'function') continue;
        if(k.match(/^_/)) continue;
        data[k] = brd[k];
      }
      return data;
    },
    methods: {
      ...proxy(brd),
      async destroy() {
        localStorage.clear();
      },
    },
  });
} else {
  Vue.prototype.$brd = new Vue({
    data() {
      return {
        options: {},
        eme: {},
        api: {},
      };
    },
    methods: {

    }
  });
};
    
