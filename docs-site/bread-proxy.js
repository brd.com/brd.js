var express = require('express');
const API_BASE = 'https://stage2.breadwallet.com';

var stream = require('stream');
var hyperquest = require('hyperquest');

function pipeOut(external, internal, response) {
  var pipe = new stream.PassThrough();

  external.writeHead(response.statusCode,response.headers)

  internal.on('data',function(f) { external.write(f); });
  internal.on('end',function(f) { external.end();  });
}

var app = new express();


app.use(async (req,res,next) => {
  var url = `${API_BASE}${req.url}`;
//  console.log("BRD -> ",url);
  var headers= { ...req.headers };
  delete headers.host;

  var forwardedFor = (req.header('x-forwarded-for') || '').split(/\s*,\s*/);
  forwardedFor.push(req.connection.remoteAddress);
  
  var date = headers['x-date'];

  const whitelist = ['x-bitcoin-testnet','authorization','accept','accept-language','accept-encoding',
                     'content-type','content-length','date','referer','user-agent',
                    ];
  
  for(var k in headers) {
    if(whitelist.indexOf(k.toLowerCase()) == -1) {
//      console.log("Dropping header ",k);
      delete headers[k];
    }
  }

  if(date) {
    headers['date'] = date;
  }

  headers['x-forwarded-for'] = forwardedFor.join(', ');

  // console.log("HEAD: ",headers);

  var options = {
    method: req.method,
    headers,
    timeout: 20000
  };

  var internal = hyperquest(url, options);
  
  if (req.method != 'GET') {
    req.pipe(internal);
  }
  
  internal.on('response',function(response) {
    pipeOut(res, internal, response);
  });

  internal.on('error', function(error,response) {
    console.log("HYPERQUEST ERROR");
    console.log('ERROR: ', error);
    res.status = 500;
    res.body = 'Error!';
  });
});

module.exports = app;


