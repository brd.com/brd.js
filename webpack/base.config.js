var webpack = require('webpack');
var path = require('path');

var config = {
  entry: {
    'brd': ['@babel/polyfill','./index.js'],
  },
  plugins: [
    new webpack.DefinePlugin({
      DEBUG: JSON.stringify(true)
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components|deps)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [['@babel/preset-env',{
              "useBuiltIns": "entry",
              "targets": "> 0.25%, not dead",
            }]],
          }
        }
      }
    ]
  },
  resolve: {
    alias: {
      // TODO: Make understudy do the basic things that we need from lodash.
      'lodash': path.resolve('./deps/understudy.js'),
      'bitcore-lib': path.resolve('./deps/bitcore-lite'),
      'bitcore-message': path.resolve('./deps/bitcore-message.js'),
      'protobufjs/minimal': path.resolve('./deps/protobufjs/index-minimal'),
    },
  },
  resolveLoader: {
    modules: [
      path.resolve(__dirname),
      path.resolve(__dirname,'../node_modules'),
    ]
  },
};

module.exports = config;
