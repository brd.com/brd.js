var glob = require('glob-fs')({ gitignore: true });
var base = require('./base.config.js');
var webpackTape = require('./tape-plugin');
var webpack = require('webpack');
var nodeExternals = require('webpack-node-externals');
var path = require('path');

var files = glob.readdirSync('examples/**/*.js');
files = files.map(f => f.match(/^.*?(examples\/.*)$/)[1]);

var base = {
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [['@babel/env',{ targets: { node: 10 }, useBuiltIns: "entry" }]],
          }
        }
      }
    ],
  },
  resolve: {
    alias: {
      'brd.js/lib/api': path.resolve('./lib/api.js'),
      'lodash': path.resolve('./deps/understudy.js'),
      'bitcore-lib': path.resolve('./deps/bitcore-lite'),
      'bitcore-message': path.resolve('./deps/bitcore-message.js'),
      'protobufjs/minimal': path.resolve('./deps/protobufjs/index-minimal'),
    },
  },
}

var nodeBase = Object.assign({},base,{
  mode: 'development',
  target: 'node',
  
  externals: [nodeExternals()],
});

var webBase = Object.assign({},base,{
  mode: 'development',
});

var things = files.map(f => {
  var entry = ['babel-polyfill','./'+f];
  var output = { filename: f };
  
  if(f.match(/server/)) {
    return Object.assign({},nodeBase,{ entry, output })
  } else {
    return Object.assign({},webBase, { entry, output })
  }
});

module.exports = things;
