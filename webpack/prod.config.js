const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var webpack = require('webpack');
var path = require('path');
var base = require('./base.config.js');

var minified = Object.assign({},base,{
  output: {
    filename: '[name].min.js',
    library: 'BRD',
    libraryTarget: 'var',
  },
  devtool: 'source-map',
  mode: 'production'
});

minified.plugins.push(new BundleAnalyzerPlugin({
  analyzerMode: 'static',
}));

var debug = Object.assign({},base,{
  output: {
    filename: '[name].js',
    library: 'BRD',
    libraryTarget: 'var',
  },
  devtool: 'source-map',
  mode: 'development',
});


module.exports = [minified,debug];
