const Parser = require('tap-parser');
const { spawn } = require('child_process')
var PassThrough = require('stream').PassThrough

function WebpackTapeRun (options) {
  this.options = options || {}
}

WebpackTapeRun.prototype.apply = function (compiler) {
  compiler.hooks.afterEmit.tapAsync('tape-plugin',run.bind(null,this.options));
}

function inParallel(list) {
  return Promise.all(list.map((fn) => new Promise((res) => fn(res))))
}

async function run (options, compilation, callback) {
  var t;
  for(var asset in compilation.assets) {
    t = compilation.assets[asset].existsAt;
  }

  var tape = spawn('tape',[t]);

  var p = new Parser();
  tape.stdout.pipe(p);

  p.on('line',r => console.log(r.match(/^(.*?)\s*$/)[1]));

  var results = await new Promise((res) => p.on('complete',res));

  if(!results.ok) {
    console.log("ERRORS: ");

    results.failures.forEach(f => {
      console.log(f.diag);
      console.log(f.diag.stack);
    });

    compilation.errors.push(new Error("Tests failed :("));

    process.exit(1);
  } else {
    console.log(`${results.pass} tests pass`);
  }
}

module.exports = WebpackTapeRun
