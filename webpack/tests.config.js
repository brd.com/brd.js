var glob = require('glob-fs')({ gitignore: true });
const util = require('util')
var base = require('./base.config.js');
var webpackTape = require('./tape-plugin');
var webpack = require('webpack');
var nodeExternals = require('webpack-node-externals');
var path = require('path');

var files = glob.readdirSync('tests/**/*.tests.js');

// cut it off before tests:
files = files.map(f => f.match(/^.*?(tests\/.*)$/)[1]);

console.log("DO files: ",files);

function presetEnv(config,callback) {
  config.module.rules.forEach(r => {
    if(r.use && r.use.loader == 'babel-loader') {
      
      var presets = r.use.options.presets;
      for(var i = 0 ; i < presets.length ; i++) {
        var preset = presets[i];
        if(typeof preset == 'string') {
          preset = [preset,{}];
        }

        if(preset.length > 1) {
          preset[1] = callback(preset[1] || {});
        }

        presets[i] = preset;
      }
    }
  });

  return config;
}

base = presetEnv(base,function(options) {
  return { targets: { node: true }};
});

console.log("BASE: ",util.inspect(base,{showHidden: false, depth: null}));

var config = Object.assign({},base,{
  mode: 'development',
  target: 'node',
  devtool: 'none',
  externals: [nodeExternals({
    whitelist:['lodash']
  })],
  entry: files.map(f => `./${f}`),
});

//config.plugins.push(new webpackTape());

// config.plugins.push(new webpack.DefinePlugin({
//   'process.client': 'false',
//   'process.server': 'true',
// }));

module.exports = config;
