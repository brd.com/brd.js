var test = require('tape');
import fetch from 'isomorphic-unfetch';
import EME , { construct, deconstruct } from '../lib/eme';

const PrivateKey = require('bitcore-lib/privatekey');

import base58 from 'bitcore-lib/encoding/base58';

const pause = (duration) => new Promise(res => setTimeout(res,duration));

const PRIVATE_KEY_1 = '4f7c75960c0e8394fa4c632d471bc4c94628775cd2f785bd61daecfe06c0c68a';
const DEVICE_1 = '22077365-0683-41b7-bbc3-cc17c9c85e3f';

const PRIVATE_KEY_2 = '78252b7a193f46099969226f9c2eb3e9cb8493b9869db274e2bd3b4aefa30516';
const DEVICE_2 = 'b4bba460-d1e5-4b45-8e6d-0ee3c6e9998c';



var PigeonProto = require("../lib/eme-proto");

const EME_OPTIONS = {
  fetch: require('isomorphic-unfetch'),
  staging: true,
};

test('test message creation',async function(t) {
  try {
    var eme1 = new EME({ privateKey: PRIVATE_KEY_1, deviceID: DEVICE_1, ...EME_OPTIONS },'PWB');
    var eme2 = new EME({ privateKey: PRIVATE_KEY_2, deviceID: DEVICE_2, ...EME_OPTIONS },'PWB');

    await eme1.ready();
    await eme2.ready();
    
    var message = construct.call(
      eme1,
      eme2.api.privateKey.toPublicKey().toDER(),
      'PWB',
      'LINK',
      { publicKey: eme1.api.privateKey.toPublicKey().toDER(),
        status: PigeonProto.messages.Status.ACCEPTED,
      },
    );

    var envelope = deconstruct.call(eme2,message);

    t.equal(envelope.message.status,PigeonProto.messages.Status.ACCEPTED,'status is unchanged');
    t.equal(base58.encode(envelope.message.publicKey),base58.encode(eme1.api.privateKey.toPublicKey().toDER()),'public key is unchanged');
  } catch(x) {
    t.error(x,'An exception occurred');
  }

  t.end();
});


test('test message end-to-end',async function(t) {
  try {
    var eme1 = new EME({ ...EME_OPTIONS },'PWB');
    var eme2 = new EME({ ...EME_OPTIONS },'PWB');

    await eme1.ready();
    await eme2.ready();

    var res = await eme1.send(
      eme2.api.privateKey.toPublicKey().toDER(),
      'LINK',
      { publicKey: eme1.api.privateKey.toPublicKey().toDER(),
        status: PigeonProto.messages.Status.ACCEPTED,
      },
    );

    await eme2.fetch();

    var entry = eme2.entries[eme2.entries.length-1];
    t.ok(entry,"message comes back on other side");
    
    var envelope = entry.envelope;

    t.equal(envelope.message.status,PigeonProto.messages.Status.ACCEPTED,'status is unchanged');
    t.equal(base58.encode(envelope.message.publicKey),base58.encode(eme1.api.privateKey.toPublicKey().toDER()),'public key is unchanged');
  } catch(x) {
    t.error(x,'An exception occurred');
  }

  t.end();
});

test('test associated keys',async function(t) {
  try {
    var eme1 = new EME({ ...EME_OPTIONS },'PWB');
    var eme2 = new EME({ ...EME_OPTIONS },'PWB');

    await eme1.ready();
    await eme2.ready();

    var associatedKey = new PrivateKey();

    eme2.associateKey(associatedKey);
    
    var res = await eme1.send(
      associatedKey.toPublicKey().toDER(),
      'LINK',
      { publicKey: eme1.api.privateKey.toPublicKey().toDER(),
        status: PigeonProto.messages.Status.ACCEPTED,
      },
    );

    await eme2.fetch();
    
    var entry = eme2.entries[eme2.entries.length-1];
    var envelope = entry.envelope;

    t.equal(envelope.message.status,PigeonProto.messages.Status.ACCEPTED,'status is unchanged');
    t.equal(base58.encode(envelope.message.publicKey),base58.encode(eme1.api.privateKey.toPublicKey().toDER()),'public key is unchanged');
  } catch(x) {
    t.error(x,'An exception occurred');
  }

  t.end();
});


test('test pushNotification signature verification',async function(t) {
  try {
    var eme1 = new EME({ ...EME_OPTIONS },'PWB');
    var eme2 = new EME({ ...EME_OPTIONS },'PWB');

    await eme1.ready();
    await eme2.ready();

    var res = await eme1.send(
      eme2.api.privateKey.toPublicKey().toDER(),
      'LINK',
      { publicKey: eme1.api.privateKey.toPublicKey().toDER(),
        status: PigeonProto.messages.Status.ACCEPTED,
      },
      { pushNotification: { message: 'foo' } }
    );

    await eme2.fetch();

    var entry = eme2.entries[eme2.entries.length-1];
    t.ok(entry,'must get message back');
    
    var envelope = entry.envelope;

    t.equal(envelope.pushNotification.message,'foo','PN message is unchanged');
    t.equal(envelope.message.status,PigeonProto.messages.Status.ACCEPTED,'status is unchanged');
    t.equal(base58.encode(envelope.message.publicKey),base58.encode(eme1.api.privateKey.toPublicKey().toDER()),'public key is unchanged');
  } catch(x) {
    t.error(x,'An exception occurred');
  }

  t.end();
});
