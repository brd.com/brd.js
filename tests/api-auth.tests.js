var test = require('tape');
var auth = require('../lib/api-authentication');
var uuid = require('uuid/v4');
var base58 = require('../deps/bitcore-lite/encoding/base58');

var PrivateKey = require('../deps/bitcore-lite/privatekey');

var fetch = require('node-fetch');

test('manual authentication against live server',async function(t) {
  var pk = new PrivateKey();
  var deviceID = uuid();

  var res = await fetch(
    `https://stage2.breadwallet.com/token`,
    { method: 'POST',
      body: JSON.stringify({pubKey: base58.encode(pk.toPublicKey().toBuffer()), deviceID }),
      headers: {
        'content-type': 'application/json',
        'accept': 'application/json',
        'x-bitcoin-testnet': 'true',
      },
    }
  );

  var token = (await res.json()).token;

  var meRequest = {
    url: 'https://stage2.breadwallet.com/me',
    body: null,
    method: 'GET',
    headers: {
      'accept': 'application/json',
      'date': (new Date()).toUTCString(),
      'x-bitcoin-testnet': 'true',
    }
  };

  meRequest.headers.authorization = auth.signHttp(pk,token,meRequest);

  var url = meRequest.url;
  delete meRequest.url;
  
  var res = await fetch(url,meRequest);
  
  t.equal(res.status,200,"Needs to get a 200 back.");
  
  if(res.status == 200) {
    var me = await res.json();
    
    t.ok(me.id,"Needs to get an ID back.");
  }

  t.end();
});
