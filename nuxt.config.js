const webpack = require('webpack');
const pkg = require('./package')
const util = require('util')
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const glob = require('glob-fs')({gitignore:true});
const Prism = require('prismjs');
const path = require('path');
const fs = require('fs');

module.exports = {
  mode: 'universal',
  srcDir: './docs-site',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },


  router: {
    extendRoutes(routes,resolve) {
      const files = glob.readdirSync('docs-site/pages/**/*.md');
      console.log("FILES: ",files);
      
      files.forEach(f => {
        var path = '/' + f.match(/pages\/(.*?).md$/)[1];
        path = path.replace(/index$/g,'');        
        var component = resolve(__dirname,f);
        
        console.log("F: ",component);
        
        if(routes.find(r => r.path == path)) return;
                
        routes.push({
          name: f.replace(/\//g,'_'),
          path: path,
          component,
        })
      })
    }
  },
  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    'plugins/components.js',
    {src: 'plugins/brd.js' },
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  serverMiddleware: [
    { path: '/bread', handler: './bread-proxy.js' },
  ],

  generate: {
    dir: 'public',
  },

  /*
  ** Build configuration
  */
  build: {
    html: {
      minify: {
        minifyJS: false,
      },
    },
    
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      if(ctx.isClient && process.env.MODERN_BROWSER == 'true') {
        config.module.rules[2].use[0].options.presets[0][1].modern = true;
      }
      
      config.resolve.alias = Object.assign({},config.resolve.alias,{
        'lodash': path.resolve('./deps/understudy.js'),
        'bitcore-lib': path.resolve('./deps/bitcore-lite'),
        'bitcore-message': path.resolve('./deps/bitcore-message.js'),
        'protobufjs/minimal': path.resolve('./deps/protobufjs/index-minimal'),
      });
      
      config.plugins = config.plugins || [];
      config.plugins.push(new webpack.DefinePlugin({
        DEFAULT_API: JSON.stringify(process.env.DEFAULT_API || 'https://api.breadwallet.com')
      }));
                          
      var oldExclude = config.module.rules[2].exclude;
      config.module.rules[2].exclude = function(file) {
        var old = oldExclude.apply(this,arguments);

        if(old) return old;

        // local deps dir:
        var deps = new RegExp(path.resolve('./deps'));
        
        var n = !!file.match(deps);
        
        return n;
      }
        
      config.module.rules.push(
        {
          test: /\.md$/,
          use: [
            {
              loader: 'vue-loader'
            },
            {
              loader: 'vue-markdown-loader/lib/markdown-compiler',
              options: {
                // markdown-it config
                raw: true,
                preset: 'default',
                breaks: true,
                highlight: function (str, language) {
                  var prismLang  = Prism.languages[language]
                  if (prismLang) {
                    try {
                      return Prism.highlight(str,prismLang);
                    } catch (__) {}
                  }

                  return ''; // use external default escaping
                },
              }
            }
          ]
        }
      );
    }
  }
}
