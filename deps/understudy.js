// WIP: the goal is to remove lodash as a dependency, as the number of
// functions in it that we need is very small, and we want to keep our
// dependency tree to zero.

module.exports = {
  each: function(iterable,callback) {
    for(var k in iterable) {
      callback(iterable[k],k);
    }
  },
  isString(thing) {
    return typeof thing == 'string';
  },
  isUndefined(thing) {
    return typeof thing == 'undefined';
  },
  isObject(thing) {
    return thing!=null && typeof thing == 'object';
  },
  isNull(thing) {
    return thing === null;
  },
}
