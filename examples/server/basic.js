import API from 'brd.js/lib/api';
import fetch from 'node-fetch';

var brd = new API({ fetch, /* optional privatekey, deviceid, token, etc... */ });

// prints: { id: 123456 }
brd.fetch('/me').then(({error,result}) => {
  if(error) console.error("Error contacting server: ",error);
  else console.log(result)
});
