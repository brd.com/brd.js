# BRD.js
[![pipeline status](https://gitlab.com/brd.com/brd.js/badges/master/pipeline.svg)](https://gitlab.com/brd.com/brd.js/commits/master)

Are you looking to add some secure crypto purchasing power to your
website or server? If so, then you've come to the right place!

BRD.js is a package for easy communication between the BRD app and
your Javascript (client or server). Authentication, sessions, and
client-side encryption are all handled in the library itself. It's
designed to be flexible; all hard requirements are inline and stripped
down for a small build, but are designed to be easily replaced with
somewhat standard components. You can use it in one of two ways,
directly included as a javascript file into HTML for simple,
bullet-proof integration, or built using Webpack into a more
customized solution.

Examples available at http://brd-js.brd.tools/

---

## Building & Running

The easiest way to build is with Docker Compose.  However, the normal
way of `npm install` and `npm run build` will also work.

---

## How to use

There are 2 objects of note: an API (this talks to BRD's backend) and
an EME (this talks to a BRD phone app).

### Linking

The BRD app is designed to link by scanning a QR code. You will need to make a QR code with a value of:

```javascript
var api = new BRD.API({ api: 'http://enable-cors.b.brd.com/bread' });
var eme = new BRD.EME(api,'<service>');
var qrCodeValue = eme.qrLinkUrl();
```

### API object

If you want information on your user, you will check with the API:

```javascript
var api = new BRD.API({ api: 'http://enable-cors.b.brd.com/bread' });
api.fetch('/me');
```

### EME object

For all messaging with the app, use the Encrypted Message Exchange (EME) object.

---

## Examples

### In a web app

First, you will need to build the code with:

```
docker-compose run app npm run-script build
```

That will spit out a file in _dist/brd.js_ for usage in your web app or on your server. Then run the example:

```
open -a Google\ Chrome tests/integration/index.html
```

### In a server

Examples may be found in the [examples directory](/examples).  You can compile & run them directly using the usual NPM method:

```
git clone git@gitlab.com:brd.com/brd.js.git
cd brd.js
npm install
npm run examples
node dist/examples/server/basic.js
```

You may also wish to compile them using Docker, which is how we do most of our environment management at [BRD](https://brd.com):

```
git clone git@gitlab.com:brd.com/brd.js.git
cd brd.js
docker-compose run app npm run examples
docker-compose run app node dist/examples/server/basic.js
```

## Architecture

There are two primary objects at the core of brd.js, API and EME.  API
is the core client to the api, and is used by the EME object in oder
to communicate.  The EME object (encrypted message exchange) is there
to interact with the server-side EME service (aka pigeon).

## Authentication
At it's most basic, brd.js provides authentication to the backend, which has a much higher bar in terms of participation.  You need to have a private/public key pair and a token registered with the backend.  This allows the backend to verify the identity of every call.  There is no way to "spoof" without access to the private key, which is obviously never sent over the wire.

The basic mechanism of the authentication is in [api-authentication.js](/lib/api-authentication.js), and can be seen, without any hand-holding in [api-auth.tests.js](/tests/api-auth.tests.js).  Automatic authentication is handled by [api.js](/lib/api.js), which you can use to instantly communicate with the backend:

```js
import API from 'brd.js/lib/api';

var brd = new API({ /* optional privatekey, deviceid, token, etc... */ });

// prints: { result: { id: 123456 } }
console.log(await brd.fetch('/me'));
```

## Encrypted Message Exchange (EME)

The EME is a way of sending messages to specific clients, requesting
actions like transactions.  To see an example of how you might set up
a pairing environment and send, see
[tests/integration/index.html](/tests/integration/index.html).  If you
want to run it on your local machine, make sure to build first:

```
# with docker:
docker-compose run app npm run build
# directly:
npm run build
```


## Lifecycle

By default, the API object will store credentials in localStorage so
that reloading doesn't delete your user and environment.  On the
server-side, you are expected to do this manually, as server-side
environments have less predictable persistence strategies.  Either
way, you may wish to "end" an API object's life, prevent future
requests, and trash any authentication mechanism.  To do so, use the
`destroy()` method.  Like so:

```
api.destroy();
```

This will prevent any further authenticated requests from going out,
delete any persisted user data, and clean up anything stored by the
api object.  If you reload the page or create a new API object, it
will generate a new authentication environment, and talk to the server
using a brand new user.
